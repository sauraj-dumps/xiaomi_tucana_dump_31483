## tucana-user 11 RKQ1.200826.002 V13.0.1.0.RFDMIXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: tucana
- Brand: Xiaomi
- Flavor: tucana-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V13.0.1.0.RFDMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/tucana/tucana:11/RKQ1.200826.002/V13.0.1.0.RFDMIXM:user/release-keys
- OTA version: 
- Branch: tucana-user-11-RKQ1.200826.002-V13.0.1.0.RFDMIXM-release-keys
- Repo: xiaomi_tucana_dump_31483


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
